import { Component, OnInit } from '@angular/core';
import { UserDashboardService } from '../../user-dashboard.service';
import { User } from '../../interfaces/user-dashboard.interface';

@Component({
  selector: 'user-dashboard',
  styleUrls: ['./user-dashboard.component.scss'],
  template: `
    <div>
      <user-overview
        *ngFor="let user of users"
        [info]="user"
        (detail)="handleDetail($event)"
      ></user-overview>
      <user-details
        [active]="isDetailActive"
        (close)="handleClose($event)"
        [userInfo]="user"
      ></user-details>
    </div>
  `,
})
export class UserDashboardComponent implements OnInit {
  users: User[];
  isDetailActive: boolean = false;
  user: User;
  constructor(private userService: UserDashboardService) {}
  ngOnInit() {
    this.userService
      .getUsers()
      .subscribe((data: User[]) => (this.users = data));
  }

  handleDetail(event: User) {
    this.isDetailActive = true;
    this.user = event;
  }

  handleClose(event: boolean) {
    if (event) {
      this.isDetailActive = false;
    }
  }
}
