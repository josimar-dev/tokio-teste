import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { User } from '../../interfaces/user-dashboard.interface';

@Component({
  selector: 'user-details',
  styleUrls: ['./user-details.component.scss'],
  template: `
    <div class="user-details" [class.active]="active">
      <div class="user-details-content">
        <span (click)="onClose()" class="user-details-content-close">X</span>
        <h3>{{ userInfo ? userInfo.name : '' }}</h3>
        <p>Telefone: {{ userInfo ? userInfo.phone : '' }}</p>
        <p>Site: {{ userInfo ? userInfo.website : '' }}</p>
        <p>Nome da empresa: {{ userInfo ? userInfo.company.name : '' }}</p>
        <div class="user-details-content-map">
          <google-map></google-map>
        </div>
      </div>
    </div>
  `,
})
export class UserDetailsComponent implements OnInit {
  @Input() active: boolean;
  @Input() userInfo: User;
  @Output() close: EventEmitter<any> = new EventEmitter();
  zoom = 12;
  center: google.maps.LatLngLiteral;
  options: google.maps.MapOptions = {
    mapTypeId: 'hybrid',
    zoomControl: false,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    maxZoom: 15,
    minZoom: 8,
  };

  ngOnInit() {
    if (this.userInfo) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.center = {
          lat: parseInt(this.userInfo.address.geo.lat),
          lng: parseInt(this.userInfo.address.geo.lng),
        };
      });
    }
  }

  onClose() {
    this.close.emit(this.active);
  }
}
