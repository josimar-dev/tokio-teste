import { Component, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../interfaces/user-dashboard.interface';

@Component({
  selector: 'user-overview',
  styleUrls: ['./user-overview.component.scss'],
  template: `
    <div class="overview">
      <div class="overview-info">
        <span>Nome: {{ info.name }}</span>
        <span>E-mail: {{ info.email }}</span>
      </div>
      <button (click)="showDetails()">Ver Detalhes</button>
    </div>
  `,
})
export class UserOverviewComponent {
  @Input() info: User;
  @Output() detail: EventEmitter<any> = new EventEmitter();

  showDetails() {
    this.detail.emit(this.info);
  }
}
