import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { GoogleMapsModule } from '@angular/google-maps';

// Containers
import { UserDashboardComponent } from './containers/user-dashboard/user-dashboard.component';

// Presentational
import { UserOverviewComponent } from './presentational/user-overview/user-overview.component';
import { UserDetailsComponent } from './presentational/user-details/user-details.component';

// Service
import { UserDashboardService } from './user-dashboard.service';

@NgModule({
  declarations: [
    UserDashboardComponent,
    UserOverviewComponent,
    UserDetailsComponent,
  ],
  imports: [CommonModule, HttpClientModule, GoogleMapsModule],
  exports: [UserDashboardComponent],
  providers: [UserDashboardService],
})
export class UserDashboardModule {}
